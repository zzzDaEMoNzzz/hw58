import React, { Component } from 'react';
import './App.css';
import Modal from "./components/Modal/Modal";
import Alert from "./components/UI/Alert/Alert";

class App extends Component {
  state = {
    modalShow: false
  };

  modalShow = () => {
    this.setState({modalShow: true});
  };

  modalClosed = () => {
    this.setState({modalShow: false});
  };

  modalContinued = () => {
    alert('Continued');
  };

  alertDismiss = () => {
    alert('Dismissed!');
  };

  render() {
    return (
      <div className="App">
        <Modal
          title="Test Title"
          show={this.state.modalShow}
          closed={this.modalClosed}
          buttons={[
            {type: 'primary', label: 'Continue', clicked: this.modalContinued},
            {type: 'danger', label: 'Close', clicked: this.modalClosed}
          ]}
        >
          <p>Lorem ipsum dolor sit amet.</p>
        </Modal>
        <Alert type="primary" clickDismissable dismiss={this.alertDismiss}>Primary type `clickDismissable` alert</Alert>
        <Alert type="success" dismiss={this.alertDismiss}>Success type alert with dismiss button</Alert>
        <Alert type="danger">Danger type alert</Alert>
        <Alert type="warning">Warning type alert</Alert>

        <button onClick={this.modalShow}>Show Modal</button>

      </div>
    );
  }
}

export default App;
