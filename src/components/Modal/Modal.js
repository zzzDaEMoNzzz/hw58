import React, {Fragment} from 'react';
import './Modal.css';
import Backdrop from "../Backdrop/Backdrop";
import Button from "../UI/Button/Button";

const Modal = props => {
  const buttonsArray = props.buttons.reduce((acc, button) => {
    acc.push (
      <Button
        type={button.type}
        clicked={button.clicked}
      >
        {button.label}
      </Button>
    );

    return acc;
  }, []);

  return (
    <Fragment>
      <Backdrop show={props.show} clicked={props.closed} />
      <div className="Modal" style={{
        transform: props.show ? 'translateY(0)' : 'translateY(-100vh)',
        opacity: props.show ? '1' : '0'
      }}>
        <div className="ModalTitle">
          <h3>{props.title}</h3>
          <button onClick={props.closed}>X</button>
        </div>
        {props.children}
        <div className="ModalButtons">
          {buttonsArray}
        </div>
      </div>
    </Fragment>
  );
};

export default Modal;
