import React from 'react';
import './Alert.css';

const Alert = props => {
  let dismissBtn = null;
  if (props.dismiss && !props.clickDismissable) {
    dismissBtn = <button className="dismissBtn" onClick={props.dismiss}>Close</button>;
  }

  const alertClassNames = [
    'Alert',
    props.type
  ];

  if (props.clickDismissable && props.dismiss) {
    alertClassNames.push('clickDismissable');
  }

  return (
    <div
      className={alertClassNames.join(' ')}
      onClick={(props.clickDismissable && props.dismiss) ? props.dismiss : null}
    >
      {props.children}
      {dismissBtn}
    </div>
  );
};

export default Alert;
